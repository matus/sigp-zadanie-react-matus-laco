import {all, call, put, select, takeEvery, takeLatest} from 'redux-saga/effects'
import {fetchAsync, fetchByIds, getSearchResultsUrl, parseMovieDetailResponse, parseSearchResult} from "../utils";
import {
    ACTION_TYPE,
    LoadByIdsRequestAction,
    LoadFavouritesRequestAction,
    Movie,
    Movies,
    SearchRequestAction,
    SearchResultAction
} from "../reducers/MoviesAppTypes";
import {favouritesIdsSelector, moviesWithDetailsIdsSelector} from "../reducers/MoviesAppSelectors";
import {AxiosResponse} from "axios";

const delay = (ms: number) => new Promise(res => setTimeout(res, ms))


/**
 * Issues API http request to load search results
 *
 * @param action
 */
function* handleSearchRequest(action: SearchRequestAction) {

    // simulate network latency
    yield delay(500);

    // construct url
    const pageId = action.page || 1
    const url = getSearchResultsUrl(action.query, {page: pageId})

    // make search request
    try {
        const result = parseSearchResult(yield fetchAsync(url));
        yield put({type: ACTION_TYPE.SEARCH_MOVIES_SUCCESS, data: result, page: pageId});
    } catch (e) {
        yield put({type: ACTION_TYPE.SEARCH_MOVIES_FAILURE, data: e.message || 'Network error'});
    }
}

function* handleNextPageRequest() {

    const query = yield select(state => state.searchQuery);
    const currentPage = yield select(state => state.currentPage);
    const page = currentPage + 1

    yield handleSearchRequest({
        type: ACTION_TYPE.SEARCH_MOVIES_REQUEST,
        query,
        page
    })

}

function* handleLoadFavouritesRequest(action: LoadFavouritesRequestAction) {
    const favouriteIds = yield select(favouritesIdsSelector);
    if (favouriteIds.length) {
        yield  put({type: ACTION_TYPE.LOAD_BY_IDS_REQUEST, ids: favouriteIds})
    }
}

/**
 * Issues API requests to load movie details by provided ids
 *
 * @param action
 */
function* handleLoadByIdsRequest(action: LoadByIdsRequestAction) {

    // already loaded movies
    const idsLoaded = yield select(moviesWithDetailsIdsSelector);

    // filter out loaded movies
    const idsToLoad = action.ids.filter((id) => (!idsLoaded.includes(id)))
    if (idsToLoad.length === 0) {
        // ids already in state
        return;
    }

    // make request(s)
    try {
        //const result = parseSearchResult(yield fetchAsync(url));
        const result = yield  call(fetchByIds, idsToLoad);
        const parsed = result
            .map((item: AxiosResponse) => parseMovieDetailResponse(item))
            // convert array to object indexed by ids
            .reduce((acc: Movies, item: Movie) => ({...acc, [item.id]: item}), {})
        yield put({type: ACTION_TYPE.LOAD_BY_IDS_SUCCESS, data: parsed});
    } catch (e) {
        yield put({type: ACTION_TYPE.LOAD_BY_IDS_FAILURE, data: e.message || 'Network error'});
    }
}


function* handleLoadMoviesSuccess(action: SearchResultAction<ACTION_TYPE.SEARCH_MOVIES_SUCCESS>) {
}

function* handleLoadMoviesFailure(action: SearchResultAction<ACTION_TYPE.SEARCH_MOVIES_FAILURE>) {
}


/**
 * ------------------------- Action watchers -------------------------
 */


function* searchRequest() {
    yield takeEvery(ACTION_TYPE.SEARCH_MOVIES_REQUEST, handleSearchRequest)
}

function* nextPageRequest() {
    yield takeLatest(ACTION_TYPE.SEARCH_RESULTS_NEXT_PAGE, handleNextPageRequest)
}

function* loadFavouritesRequest() {
    yield takeEvery(ACTION_TYPE.LOAD_FAVOURITES, handleLoadFavouritesRequest)
}

function* loadByIds() {
    yield takeEvery(ACTION_TYPE.LOAD_BY_IDS_REQUEST, handleLoadByIdsRequest)
}


function* watchLoadDataSuccess() {
    yield takeEvery(ACTION_TYPE.SEARCH_MOVIES_SUCCESS, handleLoadMoviesSuccess)
}

function* watchLoadDataFailure() {
    yield takeEvery(ACTION_TYPE.SEARCH_MOVIES_FAILURE, handleLoadMoviesFailure)
}


// notice how we now only export the rootSaga
// single entry point to start all Sagas at once
export function* rootSaga() {
    yield all([
        searchRequest(),
        nextPageRequest(),
        loadFavouritesRequest(),
        loadByIds(),
        watchLoadDataSuccess(),
        watchLoadDataFailure(),
    ])
}
