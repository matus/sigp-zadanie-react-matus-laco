import {createSelector} from 'reselect'
import {Movie, Movies, MoviesAppState} from "./MoviesAppTypes";

/**
 * Returns search results as array of Movie objects
 */
export const searchResultsSelector = createSelector<MoviesAppState, Movies, string[], string[], Movie[]>(
    state => state.movies,
    state => state.searchResultsById,
    state => state.favouritesById,
    (allMovies, currentSearchResultsIds, favouritesIds) => {
        return Object.keys(allMovies)
            .filter((id) => (currentSearchResultsIds.includes(id)))
            .map((id) => {
                return {...allMovies[id], isFavourite: favouritesIds.includes(id)}
            })
    }
);


/**
 *  Returns favourites Movies as array of Movie objects
 */
export const favouritesSelector = createSelector<MoviesAppState, Movies, string[], Movie[]>(
    state => state.movies,
    state => state.favouritesById,
    (allMovies, favouritesIds) => {
        return Object.keys(allMovies)
            .filter((id) => (favouritesIds.includes(id)))
            .map((id) => {
                return {...allMovies[id], isFavourite: true}
            })
    }
);


/**
 *  Returns users favouritesi movie Ids
 */
export const favouritesIdsSelector = createSelector<MoviesAppState, string[], string[]>(
    state => state.favouritesById,
    (favouritesIds) => (favouritesIds)
);

/**
 *  Returns ids of movies, that have detailed data loaded
 */
export const moviesWithDetailsIdsSelector = createSelector<MoviesAppState, Movies, string[]>(
    state => state.movies,
    (movies) => {
        return Object.keys(movies)
            .filter((id) => (!!movies[id].details))
    }
);


/**
 * Returns movie object by id, also with favourite status
 *
 * @param id
 */
export const createMovieByIdSelector = (id: string) => createSelector<MoviesAppState, Movies, string[], Movie>(
    state => state.movies,
    state => state.favouritesById,
    (movies, favouriteIds) => {
        const movie = movies[id]
        const isFavourite = favouriteIds.includes(id)
        return {...movie, isFavourite}
    }
)
