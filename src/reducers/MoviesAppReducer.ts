import {
    ACTION_TYPE,
    LoadByIdsRequestAction,
    LoadByIdsResponseAction,
    MoviesAppState,
    SearchNextPageAction,
    SearchRequestAction,
    SearchRequestStatus,
    SearchResultAction,
    ToggleFavouriteAction
} from "./MoviesAppTypes";

const LS_KEY_FAVOURITES = 'favs'

/**
 * Initial state
 */
const getInitialState = (): MoviesAppState => {

    return {
        movies: {},
        favouritesById: getFavs(),
        searchResultsById: [],
        searchQuery: '',
        networkRequestErrorMessage: '',
        searchResultsTotal: 0,
        searchRequestStatus: SearchRequestStatus.IDLE,
        currentPage: 1,
    }
}

/**
 * Read Favourites from local storage
 */
const getFavs = (): string[] => {
    let favs = [];
    try {
        let stored = localStorage.getItem(LS_KEY_FAVOURITES);
        if (stored) {
            favs = JSON.parse(stored)
        }
    } catch (e) {
        // no action
    }
    return favs
}

type MoviesAppAction =
    SearchRequestAction
    | SearchNextPageAction
    | ToggleFavouriteAction
    | SearchResultAction<ACTION_TYPE.SEARCH_MOVIES_SUCCESS>
    | SearchResultAction<ACTION_TYPE.SEARCH_MOVIES_FAILURE>
    | LoadByIdsRequestAction
    | LoadByIdsResponseAction<ACTION_TYPE.LOAD_BY_IDS_SUCCESS>
    | LoadByIdsResponseAction<ACTION_TYPE.LOAD_BY_IDS_FAILURE>


/**
 * Main Movie App Reducer
 *
 * @param initialState
 * @param action
 */
export const moviesAppReducer = (state: MoviesAppState = getInitialState(), action: MoviesAppAction) => {


    switch (action.type) {

        case ACTION_TYPE.SEARCH_MOVIES_REQUEST:
            return {
                ...state,
                networkRequestErrorMessage: '', // reset error message
                searchResultsTotal: 0,
                searchResultsById: [],         // reset search results
                searchRequestStatus: SearchRequestStatus.IN_PROGRESS, // network status
                searchQuery: action.query,
                currentPage: 1
                // movies:  to improve: nechat iba favourites v stave, ked je novy search request
            }

        case ACTION_TYPE.SEARCH_RESULTS_NEXT_PAGE:
            return {
                ...state,
                searchRequestStatus: SearchRequestStatus.IN_PROGRESS,
            }

        case ACTION_TYPE.SEARCH_MOVIES_FAILURE:
            return {
                ...state,
                networkRequestErrorMessage: action.data,
                searchRequestStatus: SearchRequestStatus.COMPLETED_FAILURE,
            }

        case ACTION_TYPE.SEARCH_MOVIES_SUCCESS:
            return {
                ...state,
                movies: {...state.movies, ...action.data.movies},
                searchResultsTotal: action.data.total,
                searchResultsById: [...state.searchResultsById, ...Object.keys(action.data.movies)],
                searchRequestStatus: SearchRequestStatus.COMPLETED_OK,
                currentPage: action.page ? action.page : state.currentPage,
            }

        case ACTION_TYPE.TOGGLE_FAVOURITE:

            // add or remove favourite id
            const newFavourites = state.favouritesById.includes(action.id) ? state.favouritesById.filter(id => id !== action.id) : [...state.favouritesById, action.id];

            // save to storage to persist browser reload
            localStorage.setItem(LS_KEY_FAVOURITES, JSON.stringify(newFavourites));

            return {
                ...state,
                favouritesById: newFavourites
            }

        case ACTION_TYPE.LOAD_BY_IDS_REQUEST:
            return {
                ...state,
                networkRequestErrorMessage: ''
            }

        case ACTION_TYPE.LOAD_BY_IDS_FAILURE:
            return {
                ...state,
                networkRequestErrorMessage: action.data
            }

        case ACTION_TYPE.LOAD_BY_IDS_SUCCESS:
            return {
                ...state,
                movies: {...state.movies, ...action.data}
            }
    }
    ;

    return state;
};