/**
 * Shape of the application state
 */
import {Movie} from "@material-ui/icons";

export type MoviesAppState = {
    movies: Movies,
    favouritesById: string[],
    searchResultsById: string[],
    networkRequestErrorMessage: string,
    searchRequestStatus: SearchRequestStatus,
    searchResultsTotal: number,
    searchQuery: string,
    currentPage: number
};
/**
 * Shape of Movies list object
 */
export type Movies = { [id: string]: Movie };

/**
 * Shape of Movie object
 */
export type Movie = {
    id: string,
    Year: string,
    Type: string,
    Title: string,
    imdbID: string,
    Poster: string,
    isFavourite?: boolean,
    details?: any, // all other data
    updatedAt?: number,
};

/**
 * Action types
 */
export enum ACTION_TYPE {
    SEARCH_MOVIES_REQUEST = 'SEARCH_MOVIES_REQUEST',
    SEARCH_MOVIES_SUCCESS = 'SEARCH_MOVIES_SUCCESS',
    SEARCH_MOVIES_FAILURE = 'SEARCH_MOVIES_FAILURE',
    SEARCH_RESULTS_NEXT_PAGE = 'SEARCH_RESULTS_NEXT_PAGE',
    TOGGLE_FAVOURITE = 'TOGGLE_FAVOURITE',
    LOAD_BY_IDS_REQUEST = 'LOAD_BY_IDS_REQUEST',
    LOAD_BY_IDS_SUCCESS = 'LOAD_BY_IDS_SUCCESS',
    LOAD_BY_IDS_FAILURE = 'LOAD_BY_IDS_FAILURE',
    LOAD_FAVOURITES = 'LOAD_FAVOURITES',
}

export enum SearchRequestStatus {
    IDLE,
    IN_PROGRESS,
    COMPLETED_OK,
    COMPLETED_FAILURE
}

export type SearchResultsResponse = {
    movies: Movies,
    total: number,
}
/**
 * Action Types
 */

export type SearchRequestAction = {
    type: ACTION_TYPE.SEARCH_MOVIES_REQUEST,
    query: string,
    page?: number,
}

export type SearchNextPageAction = {
    type: ACTION_TYPE.SEARCH_RESULTS_NEXT_PAGE,
}

export type SearchResultAction<T> = {
    type: T,
    // type of data  depends if type of action is success or failure
    data: T extends ACTION_TYPE.SEARCH_MOVIES_FAILURE ? string : T extends ACTION_TYPE.SEARCH_MOVIES_SUCCESS ? SearchResultsResponse : never,
    page?: number
}

export type ToggleFavouriteAction = {
    type: ACTION_TYPE.TOGGLE_FAVOURITE,
    id: string
}

export type LoadFavouritesRequestAction = {
    type: ACTION_TYPE.LOAD_FAVOURITES,
}

export type LoadByIdsRequestAction = {
    type: ACTION_TYPE.LOAD_BY_IDS_REQUEST,
    ids: string[]
}

export type LoadByIdsResponseAction<T> = {
    type: T
    data: T extends ACTION_TYPE.LOAD_BY_IDS_FAILURE ? string : Movies
}
