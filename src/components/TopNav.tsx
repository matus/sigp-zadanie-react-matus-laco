import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import {useLocation} from "react-router";
import {Link} from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            marginBottom: '20px',
        },
        menuButton: {
            margin: theme.spacing(2),
            color: 'white',
            border: '1px solid white',
            borderRadius: '3px',
            padding: '12px',
            textTransform: 'uppercase',
            fontFamily: 'Roboto',
            minWidth: '100px',
            textAlign: 'center',
            textDecoration: 'none'
        },
        menuButtonActive: {
            backgroundColor: 'white',
            color: 'black',
        },
        title: {
            flexGrow: 1,
        },
    }),
);

export function TopNav() {
    const classes = useStyles();
    const path = useLocation().pathname

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <Link to="/search"
                          className={(path === '/search') ? `${classes.menuButton} ${classes.menuButtonActive}` : classes.menuButton}>
                        Search
                    </Link>
                    <Link to="/favourites"
                          className={(path === '/favourites') ? `${classes.menuButton} ${classes.menuButtonActive}` : classes.menuButton}>
                        Favourites
                    </Link>
                </Toolbar>
            </AppBar>
        </div>
    );
}
