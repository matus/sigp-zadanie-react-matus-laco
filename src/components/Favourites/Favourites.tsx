import React, {FunctionComponent, useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {favouritesSelector} from "../../reducers/MoviesAppSelectors";
import {ItemsList} from "..";
import {ACTION_TYPE} from "../../reducers/MoviesAppTypes";
import {Link} from "react-router-dom";

export const Favourites: FunctionComponent<any> = () => {

    const items = useSelector(favouritesSelector)
    const dispatch = useDispatch()

    useEffect(
        () => {
            // run only once, when mounted
            dispatch({type: ACTION_TYPE.LOAD_FAVOURITES})
        }, [dispatch]
    );

    if (items.length === 0) {
        return (
            <div>
                No favourites... <Link to={'/search'}>Search</Link> for some movies and click the hearth icon to add a
                movie to your favourites!
            </div>
        );
    }

    return <ItemsList items={items}/>;
}
