import React, {FunctionComponent} from 'react';
import {Movie} from "../../reducers/MoviesAppTypes";
import Grid from '@material-ui/core/Grid';
import {ListItem} from "..";

type ListProps = {
    items: Movie[]
}

export const ItemsList: FunctionComponent<ListProps> = ({items}) => {

    return (
        <Grid item xs={12} style={{marginTop: '20px'}}>
            <Grid container justify={"flex-start"} spacing={2}>

                {items.map((item) => (
                    <Grid key={item.id} item={true}>
                        <ListItem item={item}/>
                    </Grid>
                ))}
            </Grid>

        </Grid>


    );
}

