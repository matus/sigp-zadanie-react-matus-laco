import React, {FunctionComponent, useEffect} from 'react';
import {useParams} from "react-router";
import {ACTION_TYPE, Movie, MoviesAppState} from "../../reducers/MoviesAppTypes";
import {useDispatch, useSelector} from "react-redux";
import {Grid} from "@material-ui/core";
import {ListItem} from "..";
import {createMovieByIdSelector} from "../../reducers/MoviesAppSelectors";
import Typography from "@material-ui/core/Typography"; // importing FunctionComponent

type ItemDetailProps = {}


export const ItemDetail: FunctionComponent<ItemDetailProps> = () => {

    let {id} = useParams();
    const dispatch = useDispatch()

    /**
     * Request to Load Movie Details on Page Load
     */
    useEffect(
        () => {
            dispatch({type: ACTION_TYPE.LOAD_BY_IDS_REQUEST, ids: [id]})
        }, [id, dispatch]
    );

    /**
     * Get item from state
     */
    const item = useSelector<MoviesAppState, Movie>(createMovieByIdSelector(id || 'not_set'))

    if (item) {
        return (
            <Grid container>
                <Grid item xs={12} md={3} style={{marginBottom: '25px'}}>
                    <ListItem item={item}/>
                </Grid>
                <Grid item xs={12} md={9}>

                    <div>Title</div>
                    <Typography variant="h4">{item.Title}</Typography>

                    {(item.details) ? (
                        <>
                            <div className={"pt-20"}>
                                <div>Actors</div>
                                <Typography variant="h5">{item.details.Actors || 'n/a'}</Typography>
                            </div>

                            <div className={"pt-20"}>
                                <div>Awards</div>
                                <Typography variant="subtitle2">{item.details.Awards || 'n/a'}</Typography>
                            </div>

                            <div className={"pt-20"}>
                                <div>Plot</div>
                                <Typography variant="body1">{item.details.Plot || 'n/a'}</Typography>
                            </div>

                            <div className={"pt-20"}>
                                <div>Runtime</div>
                                <Typography variant="body1">{item.details.Runtime || 'n/a'}</Typography>
                            </div>

                            <div className={"pt-20"}>
                                <div>Genre</div>
                                <Typography variant="body1">{item.details.Genre || 'n/a'}</Typography>
                            </div>


                            <div className={"pt-20"}>
                                <div>Country</div>
                                <Typography variant="body1">{item.details.Country || 'n/a'}</Typography>
                            </div>
                        </>
                    ) : (
                        <div>Loading details...</div>
                    )}
                </Grid>
            </Grid>
        );
    } else {
        return (<div>Loading...</div>);
    }
}

/*
This data is available in item.details
{
  "Metascore" : "N\/A",
  "Title" : "Ship of Theseus",
  "BoxOffice" : "N\/A",
  "Website" : "N\/A",
  "Awards" : "12 wins & 10 nominations.",
  "Director" : "Anand Gandhi",
  "imdbRating" : "8.0",
  "DVD" : "N\/A",
  "Response" : "True",
  "Actors" : "Aida Elkashef, Yogesh Shah, Faraz Khan, Hannan Youssef",
  "Year" : "2012",
  "Plot" : "The film explores questions of identity, justice, beauty, meaning and death through an experimental photographer, an ailing monk and a young stockbroker.",
  "imdbVotes" : "5,977",
  "Type" : "movie",
  "Language" : "English, Arabic, Swedish, Hindi",
  "Rated" : "Not Rated",
  "Runtime" : "140 min",
  "Poster" : "https:\/\/m.media-amazon.com\/images\/M\/MV5BMTU5Njc1NjkxMl5BMl5BanBnXkFtZTgwNTkzNTQwMDE@._V1_SX300.jpg",
  "Writer" : "Anand Gandhi, Anand Gandhi (story), Khushboo Ranka (story), Pankaj Kumar (story), Mahendra Shrivas (Street Play Writer)",
  "Country" : "India, Netherlands",
  "Ratings" : [
    {
      "Source" : "Internet Movie Database",
      "Value" : "8.0\/10"
    },
    {
      "Source" : "Rotten Tomatoes",
      "Value" : "100%"
    }
  ],
  "Released" : "19 Jul 2013",
  "imdbID" : "tt1773764",
  "Genre" : "Drama",
  "Pr
 */
