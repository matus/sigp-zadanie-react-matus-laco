import React, {FunctionComponent} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import {ACTION_TYPE, Movie} from "../../reducers/MoviesAppTypes";
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';
import {useDispatch} from "react-redux";
import {useHistory} from 'react-router-dom';

const useStyles = makeStyles({
    root: {
        width: 200,
        minHeight: 100,
    },
    media: {
        height: 140,
    },
    header: {
        fontSize: '22px',
        display: 'block',
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        wordBreak: 'break-all',
        wordWrap: 'break-word',
        whiteSpace: 'nowrap',
        height: '35px',
    },
});

type ItemProps = {
    item: Movie
}

export const ListItem: FunctionComponent<ItemProps> = ({item}) => {
    const classes = useStyles();
    const dispatch = useDispatch()
    const history = useHistory();

    return (
        <Card className={classes.root}>
            <CardActionArea onClick={() => history.push(`/detail/${item.id}`)}>
                <CardMedia
                    className={classes.media}
                    image={item.Poster}
                    title={item.Title}
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2" className={classes.header}>
                        {item.Title}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {item.Year}
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
                <IconButton aria-label="add to favorites"
                            onClick={() => dispatch({type: ACTION_TYPE.TOGGLE_FAVOURITE, id: item.id})}>
                    <FavoriteIcon color={item.isFavourite ? "error" : "disabled"}/>
                </IconButton>
            </CardActions>
        </Card>
    );
}
