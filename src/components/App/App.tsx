import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import './App.css';
import {ItemDetail, SearchBox} from "..";
import {SearchResults} from "../Search/SearchResults";
import {Container, Grid} from '@material-ui/core';
import {Favourites} from "../Favourites/Favourites";
import Alert from '@material-ui/lab/Alert';
import {useSelector} from "react-redux";
import {MoviesAppState} from "../../reducers/MoviesAppTypes";
import {TopNav} from "../TopNav";

export const App = () => {

    const networkError = useSelector<MoviesAppState, string>(state => state.networkRequestErrorMessage);

    return (
        <Container maxWidth={"md"}>

            <TopNav/>

            {(networkError) ? (
                <Alert severity="error" style={{marginBottom: '10px'}}>{networkError}</Alert>
            ) : (null)}

            <Switch>
                <Route path="/detail/:id">
                    <ItemDetail/>
                </Route>
                <Route path="/favourites">
                    <Favourites/>
                </Route>
                <Route path="/search">
                    <Grid container>
                        <Grid item xs={12}>
                            <SearchBox/>
                        </Grid>
                        <SearchResults/>
                    </Grid>
                </Route>
                <Redirect exact from="/" to="/search"/>
            </Switch>

        </Container>
    );
};

