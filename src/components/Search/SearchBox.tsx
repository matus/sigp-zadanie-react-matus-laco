import React, {FunctionComponent, useEffect, useRef, useState} from 'react';
import {Button, TextField} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {ACTION_TYPE, MoviesAppState, SearchRequestStatus} from "../../reducers/MoviesAppTypes";
import {normalizeSearchQuery} from "../../utils";

type SearchProps = {}

export const SearchBox: FunctionComponent<SearchProps> = () => {
    /**
     * Search Input Field Ref Hook
     */
    const queryInputFieldRef = useRef(null);

    useEffect(
        () => {
            if (queryInputFieldRef && queryInputFieldRef.current) {
                (queryInputFieldRef?.current as any).focus() // we like typescript
            }
        },
        [],
    );

    const isSearchInProgress = useSelector<MoviesAppState, boolean>((state) => state.searchRequestStatus === SearchRequestStatus.IN_PROGRESS)

    /**
     * Component state Hook
     */
    const [formState, setFormState] = useState({
        error: false,
        helperText: "",
    });



    /**
     * Dispatch Hook
     */
    const dispatch = useDispatch()

    /**
     * Submit hanlder
     *
     * @param e
     */
    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        // get query
        const query = normalizeSearchQuery(getInputValue(queryInputFieldRef));

        // validate
        if (!query) {
            // invalid inut
            setFormState({
                error: true,
                helperText: 'Please enter value!',
            });
        } else {
            // valid input
            dispatch({type: ACTION_TYPE.SEARCH_MOVIES_REQUEST, query})

            setFormState({helperText: '', error: false})
        }

    }

    return (

        <form className="" noValidate={false} autoComplete="on" onSubmit={handleSubmit} action="#">
            <TextField
                disabled={isSearchInProgress}
                error={formState.error}
                id="query"
                inputRef={queryInputFieldRef}
                label="Please enter a name of the movie"
                variant="outlined"
                helperText={formState.helperText}
                style={{margin: 8, width: '95%'}}
                margin={"normal"}
                InputLabelProps={{
                    shrink: true,
                }}
            />

            <Button
                disabled={isSearchInProgress}
                color={"primary"}
                type={"submit"}
                variant={"outlined"}
                style={{margin: '10px'}}
            >
                Search
            </Button>

        </form>
    );
}


/**
 * Returns value of an input field
 *
 * @param ref
 */
const getInputValue = (ref: any): string => {
    if (ref && ref.current && ref.current.value) {
        return ref.current.value
    } else {
        return ''
    }
}


