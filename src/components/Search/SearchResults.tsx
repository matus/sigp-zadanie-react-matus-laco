import React, {FunctionComponent} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {searchResultsSelector} from "../../reducers/MoviesAppSelectors";
import {ItemsList} from "..";
import Grid from "@material-ui/core/Grid";
import {Button} from "@material-ui/core";
import {ACTION_TYPE, MoviesAppState, SearchRequestStatus} from "../../reducers/MoviesAppTypes";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        sectionButton: {
            marginTop: '20px',
            padding: '40px',
        },
        sectionLoading: {
            paddingTop: '40px',
        },
        control: {
            padding: theme.spacing(2),
        },
    }),
);

export const SearchResults: FunctionComponent<any> = () => {
    const searchResults = useSelector(searchResultsSelector)

    const dispatch = useDispatch()
    const isSearchInProgress = useSelector<MoviesAppState, boolean>((state) => state.searchRequestStatus === SearchRequestStatus.IN_PROGRESS)
    const classes = useStyles();
    const showMoreButton = useSelector<MoviesAppState, boolean>((state => {
        return state.searchResultsTotal > 0 && state.searchResultsTotal > state.searchResultsById.length
    }))

    return (
        <>
            <ItemsList items={searchResults}/>

            {(isSearchInProgress && (
                <Grid container className={classes.sectionLoading} spacing={5} justify={"center"}>
                    <div>Loading...</div>
                </Grid>
            ))}

            {(showMoreButton && (
                <Grid container className={classes.sectionButton} spacing={5} justify={"center"}>
                    <Button
                        disabled={isSearchInProgress}
                        variant={"outlined"}
                        onClick={(e) => dispatch({type: ACTION_TYPE.SEARCH_RESULTS_NEXT_PAGE})}>Load More</Button>
                </Grid>
            ))}
        </>
    );
}
