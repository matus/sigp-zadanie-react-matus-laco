import {Movie, Movies, SearchResultsResponse} from "../reducers/MoviesAppTypes";
import {AxiosResponse} from "axios";

/**
 * Returns full query url required by OMDB api to search by movies by Title
 *
 * @param query
 * @param apiKey
 * @param page
 */
export const getSearchUrl = (query: string = '', apiKey: string = 'not_set', page: number = 2): string => {
    return `https://www.omdbapi.com/?apikey=${apiKey}&s=${encodeURI(query)}&page=${page}`
}

export const getMovieDetailUrl = (id: string = '', apiKey: string = 'not_set') => {
    return `https://www.omdbapi.com/?apikey=${apiKey}&i=${id}`
}


/**
 * Formats Open Movie API response of search results to object compatible with reducer
 *
 * @param rawResponse
 * @return SearchResultsResponse
 * @throws Error
 */
export const parseOpenMovieSearchResponse = (rawResponse: AxiosResponse): SearchResultsResponse => {

    const responseData = getResponseData(rawResponse);


    // unexpected response format
    if (!responseData.Search) {
        throw new Error('Response format unknown!')
    }

    // unexpected response format: search data response is not array
    if ((typeof responseData.Search.length) === 'undefined') {
        throw new Error('Response Search data format invalid!')
    }

    // unexpected response format: search totalResults not set
    if (isNaN(responseData.totalResults)) {
        throw new Error('Response Total results format invalid!')
    }

    return {
        total: responseData.totalResults,
        movies: responseData.Search.reduce(moviesArrayToObjectReducer, {}),
    }
}

/**
 * Performs basic validation on server API response
 *
 * @param rawResponse
 * @return any
 * @throws Error
 */
export const getResponseData = (rawResponse: AxiosResponse): any => {

    // no response
    if (!rawResponse) {
        throw new Error('Response is empty!')
    }

    // unexpected response http status code
    if (rawResponse.status !== 200) {
        if (rawResponse.data.Error) {
            throw new Error('Request failed, error: ' + rawResponse.data.Error)
        }
        throw new Error('Request failed, code: ' + rawResponse.status)
    }

    // other error provided by server
    if (rawResponse.data.Error) {
        throw new Error(rawResponse.data.Error)
    }

    return rawResponse.data;
}


/**
 * Converts array of objects into an Object, where movies are indexed by their imdbID
 *
 * @param acc
 * @param current
 */
const moviesArrayToObjectReducer = (acc: Movies, current: any) => {
    const id = current["imdbID"]
    const movie = {
        ...getBaseMovieProps(current),
        updatedAt: Date.now(),
    }
    return {...acc, [id]: movie}
}

const getBaseMovieProps = (item: any): any => {
    return {
        id: item['imdbID'],
        imdbID: item['imdbID'],
        Year: item['Year'],
        Type: item['Type'],
        Title: item['Title'],
        Poster: item['Poster'],
    }
}

/**
 * Parses movie details API response
 *
 * @param rawData
 * @return MovieDetailed
 */
export const parseOpenMovieDetailResponse = (rawResponse: AxiosResponse): Movie => {
    const responseData = getResponseData(rawResponse);
    return {
        ...getBaseMovieProps(responseData),
        details: responseData,  // simply copy all data
        updatedAt: Date.now(),
    } as Movie;
}
