import {
    getSearchUrl as getOpenMovieSearchUrl,
    parseOpenMovieDetailResponse,
    parseOpenMovieSearchResponse
} from "./OpenMovieDB";
import {OMDB_API_KEY} from "../constants";
import {Movie, SearchResultsResponse} from "../reducers/MoviesAppTypes";
import {AxiosResponse} from "axios";


export {fetchAsync} from "./networkUtils";
export {fetchByIds} from "./networkUtils";

export type ApiUrlOptions = {
    page?: number
}


/**
 * Returns URL for querying backend API for search results
 *
 * @param query
 * @param options
 */
export const getSearchResultsUrl = (query: string, options: ApiUrlOptions) => {
    return getOpenMovieSearchUrl(query, OMDB_API_KEY, options.page);
}


/**
 * Normalizes the response from API server to format accepted by store/reducer
 *
 * @param rawResponse
 */
export const parseSearchResult = (rawResponse: AxiosResponse): SearchResultsResponse => {
    return parseOpenMovieSearchResponse(rawResponse);
}

export const parseMovieDetailResponse = (rawResponse: AxiosResponse): Movie => {
    return parseOpenMovieDetailResponse(rawResponse);
}


/**
 * Basic cleanup of search query
 *
 * @param q
 */
export const normalizeSearchQuery = (q: string = ''): string => {
    return q.trim().toLowerCase();
}
