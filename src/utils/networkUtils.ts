import axios from "axios";
import {getMovieDetailUrl} from "./OpenMovieDB";
import {OMDB_API_KEY} from "../constants";


/**
 * Preforms GET request to provided url
 *
 * @param url
 * @return Promise
 */
export const fetchAsync = (url: string): Promise<any> => {
    return axios.get(url);
}


export const fetchByIds = (ids: string[]): Promise<any> => {
    return Promise.all(ids.map(id => {
        return fetchAsync(getMovieDetailUrl(id, OMDB_API_KEY))
    }))
}
