import React from 'react';
import {Provider} from 'react-redux'
import {BrowserRouter as Router, Route} from "react-router-dom";
import {App} from "./components";
import {RootStore} from "./index";

type RootProps = {
    store: RootStore
};

export const Root = ({store}: RootProps) => {
    return (
        <Provider store={store}>
            <Router>
                <Route path="/:filter?" component={App}/>
            </Router>
        </Provider>
    );
};

