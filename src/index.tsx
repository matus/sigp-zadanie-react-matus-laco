import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import {moviesAppReducer} from './reducers/MoviesAppReducer'
import {Root} from "./Root";
import {applyMiddleware, createStore} from 'redux'
import createSagaMiddleware from 'redux-saga'
import {rootSaga} from './sagas'
import {createLogger} from "redux-logger";

const sagaMiddleware = createSagaMiddleware()
//let middlewares = [sagaMiddleware]


const loggerMiddleware = createLogger({})

/**
 * Main Store
 */
const store = (process.env.NODE_ENV === `development`)
    ?
    createStore(
        moviesAppReducer,
        applyMiddleware(sagaMiddleware, loggerMiddleware),
    )
    :
    createStore(
        moviesAppReducer,
        applyMiddleware(sagaMiddleware),
    );


/**
 * Init Saga
 */
sagaMiddleware.run(rootSaga);

/**
 * Type declaration for store
 */
export type RootStore = typeof store;


/**
 * Render Root Element into HTML (root div)
 */
ReactDOM.render(
    <React.StrictMode>
        <Root store={store}/>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
